#pragma once

#include <stdbool.h>
#include <stdio.h>

struct MPCameraCalibration {
        float color_matrix_1[9];
        float color_matrix_2[9];
        float forward_matrix_1[9];
        float forward_matrix_2[9];
        unsigned short illuminant_1;
        unsigned short illuminant_2;
        unsigned int hue_sat_map_dims[3];
        size_t tone_curve_length;
        float *tone_curve;
        float *hue_sat_map_data_1;
        float *hue_sat_map_data_2;
};

struct MPCameraCalibration parse_calibration_file(const char *path);

bool find_calibration(char *conffile, const char *sensor);
